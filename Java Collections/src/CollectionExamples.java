import java.util.*;


public class CollectionExamples {

    public static void main(String[] args) {

        /*
        - List
        - Set
        - Map
        - Queue
        - Tree
         */

        System.out.println("-- List --");
        List list = new ArrayList();
        list.add("All");
        list.add("dogs");
        list.add("go");
        list.add("to");
        list.add("heaven");

        // You can also add collections in bulk
        Collections.addAll(list, "maybe", "cats", "too!");

        // You can clear out a collection like this
        //list.clear();

        for (Object str : list) {
            System.out.println((String)str);
        }

        // Lists version 2 with numbers
        System.out.println("-- List 2 --");
        List<Integer> list2 = new LinkedList<Integer>();
        list2.add(-5);
        list2.add(70);
        list2.add(-22);
        list2.add(15);

        //output should be: 70
        //grabs the largest value with max()
        System.out.println("Max is: " + Collections.max(list2));

        // Trying this with the addAll() method
        // Ignores duplicates
        System.out.println("-- Set --");
        Set set = new TreeSet();
        Collections.addAll(set, "Can", "we", "go", "to", "the", "park?", "park?");

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("-- Map --");
        Map map = new HashMap();
        /*
        You can select the order of your list manually
        by mapping it where you want to go
        */
        // Ogres are like onions, they have layers
        map.put(1, "Ogres");
        map.put(2, "are");
        map.put(3, "like");
        map.put(4, "onions,");
        map.put(5, "they");
        map.put(5, "butts");
        map.put(6, "have");
        map.put(7, "layers");

        //Collections.addAll(map, 4,"they", 3,"have", 2,"layers");
        for (int i = 1; i < map.size(); i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        System.out.println("-- Queue --");
        Queue queue = new PriorityQueue();
        queue.add("Can");
        queue.add("we");
        queue.add("all");
        queue.add("Can");
        queue.add("go");
        queue.add("to");
        queue.add("Can");
        queue.add("the");
        queue.add("park?");
        queue.add("Can");

        Collections.addAll(queue, "Arthur", "Karen", "Karen");

        // Queue's alphabetize
        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("-- Tree --");
        TreeSet<String> ts1 = new TreeSet<String>();

        // add the elements to the tree
        ts1.add("Does");
        ts1.add("It");
        ts1.add("Blend?");
        ts1.add("Zebra");

        System.out.println("The Tree Set is: " + ts1);

        String check = "Henry";

        //Check if the above string exists in the TreeSet
        System.out.println("Contains " + check + " " + ts1.contains(check));

        //Print the first element
        System.out.println("First: " + ts1.first());

        // Print the last value
        System.out.println("Last: " + ts1.last());

        String val = "Zebra";

        // Find the value just greater and smaller above the string
        System.out.println("Greater " + ts1.higher(val));
        System.out.println("Lower " + ts1.lower(val));
    }
}
