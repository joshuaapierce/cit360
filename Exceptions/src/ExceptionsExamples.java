import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionsExamples {
    public static void main(String[] args) {
        try {
                Scanner input = new Scanner(System.in);
                System.out.println("Input the first number: ");
                int num1 = input.nextInt();

                System.out.println("Input the second number: ");
                int num2 = input.nextInt();

            while (true) {
                if (num2 == 0) {
                    System.out.println("Please try again");
                    System.out.println("Please enter the second number: ");
                    //num2 = input.nextInt();
                    //int output = doDivide(num1, num2);
                    break;
                } else {
                    int output = doDivide(num1, num2);
                    System.out.println(num1 + " divided by " + num2 + " is: " + output);
                }
            }
        } catch (ArithmeticException e) {
            System.out.println("Don't enter zero for number 2");
        } catch (InputMismatchException e) {
            System.out.println("Please enter a valid number");
        }
    }

    public static int doDivide(int num1, int num2) throws ArithmeticException {
        if (num2 == 0) {
            throw new ArithmeticException();
        } else {
            int output = (num1 / num2);
            return output;
        }
    }
}
