import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteDoingSomething {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool( 3);


        DoingSomething ds1 = new DoingSomething("Bob", 25, 1000);
        DoingSomething ds2 = new DoingSomething("Sally", 10, 500);
        DoingSomething ds3 = new DoingSomething("Billy", 5, 250);
        DoingSomething ds4 = new DoingSomething("Anna", 2, 100);
        DoingSomething ds5 = new DoingSomething("Pat", 1, 50);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
