public class GuessingGame extends Thread {
    private int num;
    public GuessingGame(int num) {
        this.num = num;
    }
    public void run() {
        int counter = 0;
        int bet = 0;
        do {
            bet = (int) (Math.random() * 10 + 1);
            System.out.println(this.getName() + counter);
            counter++;
        } while (bet != num);
            System.out.println("Winner! " + this.getName() + " with " + counter + " tries!");
    }
}
