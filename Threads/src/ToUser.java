public class ToUser  implements Runnable {
    private String output;

    public ToUser(String output) {
        this.output = output;
    }

    public void run() {
        while(true) {
            System.out.println(output);
        }
    }
}
