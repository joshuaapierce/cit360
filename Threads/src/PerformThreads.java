public class PerformThreads {

    public static void main(String[] args) {

        System.out.println("Starting first thread...");
        Thread gg1 = new GuessingGame(1);
        gg1.start();
        try {
            gg1.join();
        } catch (InterruptedException e ) {
            System.out.println("Thread interrupted.");
        }

        System.out.println("Starting second thread");
        Thread gg2 = new GuessingGame(9);
        gg2.start();
        try {
            gg2.join();
        } catch (InterruptedException e) {
            System.out.println("Thread ended.");
        }
    }
}
