import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONExample {
    public static String movieIsJSON(Movie movie) {

        ObjectMapper mapper = new ObjectMapper();
        String m = "";

        try {
            m = mapper.writeValueAsString(movie);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return m;
    }

    public static Movie JSONToMovie(String m) {

        ObjectMapper mapper = new ObjectMapper();
        Movie movie = null;

        try {
            movie = mapper.readValue(m, Movie.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return movie;
    }

    public static void main(String[] args) {

        Movie mov = new Movie();
        mov.setName("The Prestige");
        mov.setRank(1);
        mov.setRate(10);

        String json = JSONExample.movieIsJSON(mov);
        System.out.println(json);

        Movie mov2 = JSONExample.JSONToMovie(json);
        System.out.println(mov2);
    }

}
