public class Movie {

    private String name;
    private double rank;
    private double rating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public double getRate() {
        return rating;
    }

    public void setRate(double rating) {
        this.rating = rating;
    }

    public String toString() {
        return "Name: " + name + " || Rank: " + rank + " || Rating: " + rating + " ||";
    }
}
