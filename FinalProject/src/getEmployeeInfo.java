import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class getEmployeeInfo {

    /*
    You are the secretary for a small business and you are tasked with
    entering employee time cards
    This program will ask you how many users you'll be entering,
    their name, and hours worked.
    It will then store that info into a collection which will be converted
    into JSON on a servlet.
    That info will be pulled from the servlet and then thrown into a runnable.
    The runnable will then calculate how much money will be paid to the employee
    based on minimum wage of $15
     */

    public static void main(String[] args) throws RuntimeException {

        // Get the employee information and store in a collection
        Scanner input = new Scanner(System.in);

        // Ask how many entries to repeat the process
        System.out.println("How many entries are you submitting?");
        double howMany = input.nextInt();

        // Turn user input into a collection
        List timeCard = new ArrayList();

        for (double i = 0; i < howMany; i++) {
            // Put into for each in order to add new employees
            System.out.println("Please enter your full name");
            String fullName = input.nextLine();

            System.out.println("How many hours did you work this week?");
            int hoursWorked = input.nextInt();

            timeCard.add(fullName);
            timeCard.add(hoursWorked);
        }

        Collections.addAll(timeCard);

        input.close();
    }

}
