import java.util.List;

public class Employee {

    private String name;
    private double hoursWorked;
    private List timeCard;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getHoursWorked() {
        return hoursWorked;
    }
    public void setHoursWorked(double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }
    public List getTimeCard() {
        return timeCard;
    }
    public void setTimeCard(List timeCard) {
        this.timeCard = timeCard;
    }
}
