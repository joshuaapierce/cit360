package com.example.ServletWithTomcat10;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloServlet", value = "/HelloServlet")
public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");

        pw.println("<html><head></head><body>");

        String username = request.getParameter("userName");
        String password = request.getParameter("Password");

        if (username.equals("admin") && password.equals("Passw0rd"))
            pw.println("Login Successful, please proceed.");
        else
            pw.println("Login failed, access denied.");
        pw.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("invalid attempt");
    }
}