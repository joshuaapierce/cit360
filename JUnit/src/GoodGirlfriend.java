public class GoodGirlfriend {
    private String girlfriend;
    private boolean hungry = false;
    private boolean correct = true;

    public GoodGirlfriend(String girlfriend) {
        this.girlfriend = girlfriend;
    }

    public String getGirlfriend() {
        return girlfriend;
    }

    public boolean isHungry() {
        return hungry;
    }

    public void main(String [] args) {
        String[] food1 = new String[] {"Pizza", "Sandwich", "Mac N' Cheese"};
        String[] food2 = new String[] {"Pasta", "Tacos", "Sushi"};
        String[] foodCorrect = new String[] {"Pasta", "Tacos", "Sushi"};

        if (food2 == foodCorrect) {
            correct = true;
        } else {
            correct = false;
        }
    }

    public void gettingHungry() {
        hungry = true;
    }

    public boolean rightChoice() {
        return correct;
    }
}
