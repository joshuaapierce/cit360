import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoodGirlfriendTest {
    GoodGirlfriend quinessa = new GoodGirlfriend("Quinessa");

    @org.junit.jupiter.api.Test
    public void getGirlfriend() throws Exception {
        assertEquals("Quinessa", quinessa.getGirlfriend());
    }

    @Test
    public void testNotHungryYet() throws Exception {
        assertFalse(quinessa.isHungry());
    }

    @Test
    public void testGettingHungry() throws Exception {
        quinessa.gettingHungry();
        assertTrue(quinessa.isHungry());
    }


    @Test
    public void whatSheWants() throws Exception {
        quinessa.rightChoice();
        assertArrayEquals(String[] food2, String[] foodCorrect);
        /*
        Stopped here
        I wanted to check to see if what I, the boyfriend, could predict what the girlfriend wanted to eat
        I got stuck because I guess I had to convert the String[] arrays I set into Objects and I'm not sure if that would work
        Anyways, some tips on where to go next would help me resubmit this.
         */
    }
}